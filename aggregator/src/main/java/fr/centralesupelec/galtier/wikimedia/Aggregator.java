package fr.centralesupelec.galtier.wikimedia;

import java.time.Duration;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Logger;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.json.JSONObject;

/**
 * @author V. Galtier
 *
 * Reads the "wikipediachanges" topic, identifies and display the 5-top active regions
 * on 10-minutes tumbling windows.
 */
public class Aggregator {

	// configure logger
	private static Logger logger = null;
	static {
		System.setProperty("java.util.logging.SimpleFormatter.format",
				"[%1$tT] [%4$-7s] %5$s %n");
		logger = Logger.getLogger(Aggregator.class.getName());
	}

	public static void main(String[] args) {
		String bootstrappingServersList = args[0];
		String wikipediaTopic = args[1];
		int windowsSec = Integer.parseInt(args[2]);
		new Aggregator(bootstrappingServersList, wikipediaTopic, windowsSec);
	}


	public Aggregator(String bootstrappingServersList, String wikipediaTopic, int windowsSec) {

		KafkaConsumer<Integer, String> consumer = new KafkaConsumer(configureKafkaConsumer(bootstrappingServersList));
		// read from any partition of the topic
		// (and hopefully all! should be alone in its group)
		consumer.subscribe(Collections.singletonList(wikipediaTopic));

		/**
		 * list of the regions subject to changes along with the number of changes
		 * key: region, value: number of times this region came up in the topic
		 */
		Map<String, Integer> regions = new HashMap<>();
		
		long timeStone = System.currentTimeMillis();

		try {
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				// read from topic
				ConsumerRecords<Integer, String> records = consumer.poll(timeout);
				for (ConsumerRecord<Integer, String> record : records) {
					JSONObject jsonObjectRoot = new JSONObject(record.value());
					String region = jsonObjectRoot.getString("region");
					// new or existing region?
					if (regions.containsKey(region)) {
						regions.put(region, 1+regions.get(region));
					} else {
						regions.put(region, 1);
					}


					if (System.currentTimeMillis() - timeStone > windowsSec*1000) {
						timeStone = System.currentTimeMillis();
						displayTop(regions);
						regions = new HashMap<>();
					}
				}

			}
		} catch (Exception e) {
			logger.severe("something went wrong... " + e.getMessage());
			System.exit(1);
		} finally {
			consumer.close();
		}	
	}


	private void displayTop(Map<String, Integer> regions) {
		// sort the regions list in reverseOrder (region key with the highest value first)
		Iterator<Entry<String, Integer>> regionsIterator = regions.entrySet()
				.stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.iterator();

		int NB_REGIONS_TO_LOG = 5;
		
		int j=0;
		Entry<String, Integer> entry = null;
		
		while((regionsIterator.hasNext())&&(j<NB_REGIONS_TO_LOG)) {
			entry = regionsIterator.next();
			logger.info(entry.getKey() + "\t\t" + entry.getValue());
			j++;
		}
		logger.info("=================================");
	}


	/**
	 * Prepares configuration for the Kafka consumer.
	 * 
	 * @param bootstrappingServersList the list of Kafka bootstrapping servers
	 * @return configuration properties for the Kafka consumer
	 */
	private Properties configureKafkaConsumer(String bootstrappingServersList) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrappingServersList);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, 
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_aggregator");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning

		return consumerProperties;
	}
}


